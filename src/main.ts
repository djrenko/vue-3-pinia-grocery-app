import { createPinia } from "pinia";
import { createApp, watch } from "vue";
import { createRouter, createWebHistory } from 'vue-router'
import App from "./App.vue";
import { useMainStore } from "./store/index";
import { createI18nWithLocale } from './locale'
import routes from '~pages';
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

const router = createRouter({
  history: createWebHistory(),
  routes,
})

const pinia = createPinia();
const i18n = createI18nWithLocale("en");


const app = createApp(App);
app.use(pinia)
app.use(i18n)
app.use(router)
app.use(ElementPlus)
  .mount("#app");
