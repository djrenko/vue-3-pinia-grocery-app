import { createI18n } from 'vue-i18n'
import en from '../locales/en.json'
import ua from '../locales/ua.json'
//import zhCN from '../locales/ua.json'

const messages = {
    en,
    ua
}

const locales = Object.keys(messages)

const createI18nWithLocale = (locale: string): any => {
    return createI18n({
        legacy: false,
        globalInjection: true,
        locale,
        messages,
    })
}
export { messages, locales, createI18nWithLocale }