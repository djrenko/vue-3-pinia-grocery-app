import { generateFakeData, Item } from "@/models/item.model";
import { defineStore } from "pinia";

export type RootState = {
  items: Item[];
  langauge: string;
};

export const useMainStore = defineStore({
  id: "mainStore",
  state: () =>
  ({
    items: [],
    langauge: localStorage.getItem("lang") ?? "en",
  } as RootState),

  actions: {
    createNewItem(item: Item) {
      if (!item) return;

      this.items.push(item);
    },

    updateItem(id: string, payload: Item) {
      if (!id || !payload) return;

      const index = this.findIndexById(id);

      if (index !== -1) {
        this.items[index] = generateFakeData();
      }
    },

    deleteItem(id: string) {
      const index = this.findIndexById(id);

      if (index === -1) return;

      this.items.splice(index, 1);
    },

    findIndexById(id: string) {
      return this.items.findIndex((item) => item.id === id);
    },


    /*Lang*/

    setLangauge(
      language: string,
    ) {
      this.langauge = language;
      localStorage.setItem("lang", (language));
    },

  },
});
